﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstShip : MonoBehaviour {
	public GameObject choseShip;
	public GameObject[] ships=new GameObject[4] ;
	// Use this for initialization
	void Start() {

		switch (PlayerPrefs.GetInt("number")) {
		case 0:
			choseShip=(GameObject)Instantiate (ships [0], new Vector3 (0, 0, 0), Quaternion.identity);
			choseShip.name = "Ship";
			choseShip.GetComponent<SHipControl> ().speed = 15;
			choseShip.GetComponent<SHipControl> ().speedRot = 5;
		
			break;
		case 1:
			choseShip=(GameObject)Instantiate (ships [1], new Vector3 (0, 0, 0), Quaternion.identity);
			choseShip.name = "Ship";
			choseShip.GetComponent<SHipControl> ().speed = 20;
			choseShip.GetComponent<SHipControl> ().speedRot = 10;
			break;
		case 2:
			choseShip=(GameObject)Instantiate (ships [2], new Vector3 (0, 0, 0), Quaternion.identity);
			choseShip.name = "Ship";
			choseShip.GetComponent<SHipControl> ().speed = 30;
			choseShip.GetComponent<SHipControl> ().speedRot = 15;
			break;
		case 3:
			choseShip=(GameObject)Instantiate (ships [3], new Vector3 (0, 0, 0), Quaternion.identity);
			choseShip.name = "Ship";
			choseShip.GetComponent<SHipControl> ().speed = 35;
			choseShip.GetComponent<SHipControl> ().speedRot = 20;
			break;
		}
	}

	
	// Update is called once per frame
	void Update () {
		
	}
}
