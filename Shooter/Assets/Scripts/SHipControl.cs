﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SHipControl : MonoBehaviour {

	public  float speed;
	public  float speedRot;
	public Transform SpawnPoint;
	public GameObject health;
	private Transform m_ship;
	private GameObject m_uipanel;
	[SerializeField]
	float m_power=50f;

	void Awake()
	{
		m_uipanel = GameObject.Find ("GameOverPanel");
	}

	void Start()
	{
		m_uipanel.SetActive (false);
		m_ship = transform;
		health = GameObject.Find ("HealthBar");
	}
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.F))
			Shoot ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		if(Input.GetKey(KeyCode.A))
			m_ship.transform.Translate(Vector3.right * Time.deltaTime*-speed, Space.World);
		if(Input.GetKey(KeyCode.D))
			m_ship.transform.Translate(Vector3.right * Time.deltaTime*speed, Space.World);
		if(Input.GetKey(KeyCode.W))
			m_ship.transform.Translate(Vector3.up * Time.deltaTime*speed, Space.World);
		if(Input.GetKey(KeyCode.Z))
			m_ship.transform.Translate(Vector3.up * Time.deltaTime*-speed, Space.World);
	


		Vector2 direction = Camera.main.ScreenToWorldPoint (Input.mousePosition) - transform.position;
		float angle = Mathf.Atan2 (direction.y,direction.x)*Mathf.Rad2Deg-90f;
		Quaternion rot = Quaternion.AngleAxis (angle,Vector3.forward);
		transform.rotation = Quaternion.Slerp (transform.rotation, rot, speedRot * Time.deltaTime);
	}
	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "Enemy") {
			if (health.GetComponent<Image> ().fillAmount <= 0) {
				GameOver (m_uipanel);
			}
			col.gameObject.SetActive (false);
			if(PlayerPrefs.GetInt("number")==0)
			health.GetComponent<Image>().fillAmount -= 0.3f;
			if(PlayerPrefs.GetInt("number")==1)
				health.GetComponent<Image>().fillAmount -= 0.2f;
			if(PlayerPrefs.GetInt("number")==2)
				health.GetComponent<Image>().fillAmount -= 0.1f;
			if(PlayerPrefs.GetInt("number")==3)
				health.GetComponent<Image>().fillAmount -= 0.1f;
		}
	}
	void Shoot() {
		GameObject obj = PoolerObject.poolled.GetPoolBullet ();
		if (obj == null)
			return;
		obj.transform.position = SpawnPoint.transform.position;
		obj.transform.rotation = SpawnPoint.rotation;
		obj.SetActive (true);
		obj.GetComponent<Rigidbody> ().velocity = transform.up * m_power;
	}
	void GameOver(GameObject uipanel)
	{
		uipanel.SetActive (true);
		Time.timeScale = 0f;
	}
}
