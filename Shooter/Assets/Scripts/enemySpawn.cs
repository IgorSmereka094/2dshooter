﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemySpawn : MonoBehaviour {
	public GameObject enemies;
	public float timeSpawn;
	public Vector3 spawnBorder;
	public float waiting;

	// Use this for initialization
	void Start () {

		StartCoroutine (SpawnEnemy());
		StartCoroutine (SpawnEnemy2());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	IEnumerator SpawnEnemy()
	{
		yield return new WaitForSeconds (timeSpawn);
		for (int i = 0; i < 100; i++) {
			
				Vector3 spawnPosition = new Vector3 (Random.Range (-50, -70), Random.Range (-spawnBorder.y, spawnBorder.y), 0);
			GameObject obj = PoolerObject.poolled.GetPoolShip ();
			if (obj == null)
				yield return null;
			obj.transform.position = spawnPosition;
			obj.transform.rotation = Quaternion.identity;

			obj.SetActive (true);
			//	Instantiate (enemies, spawnPosition, Quaternion.identity);
				yield return new  WaitForSeconds (waiting);

			}
		yield return new WaitForSeconds (20);
	}
	IEnumerator SpawnEnemy2()
	{
		yield return new WaitForSeconds (timeSpawn+0.5f);
		for (int i = 0; i < 100; i++) {
			Vector3 spawnPosition = new Vector3 (Random.Range (50, 60), Random.Range (-spawnBorder.y, spawnBorder.y), 0);
			GameObject obj = PoolerObject.poolled.GetPoolShip ();
			if (obj == null)
				yield return null;
			obj.transform.position = spawnPosition;
			obj.transform.rotation = transform.rotation;
			obj.SetActive (true);
			//Instantiate (enemies, spawnPosition, Quaternion.identity);
			yield return new  WaitForSeconds (waiting+Random.Range(1,4));

		}
	}



		
}
