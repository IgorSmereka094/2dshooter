﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolerObject : MonoBehaviour {
	public static PoolerObject poolled;
	public GameObject objShoot;
	public GameObject objShip;
	public List<GameObject> pooledObjects;
	public List<GameObject> pooledShips;
	[SerializeField]
	private GameObject m_CreateBullet;
	[SerializeField]
	private GameObject m_CreateShips;
	private int m_shipValues=100;
	private int m_bulletsValues=50;
	// Use this for initialization
	void Awake()
	{
	poolled = this;
	}
	void Start () {
		CreateShips ();
		CreateBullets ();


	}
	void CreateBullets()
	{
		pooledObjects = new List<GameObject> ();

		for (int i = 0; i < m_bulletsValues; i++) {
			GameObject obj = (GameObject)Instantiate (objShoot);
			obj.SetActive (false);
			obj.transform.SetParent (m_CreateBullet.transform);
			pooledObjects.Add (obj);
		}
	}
	// Update is called once per frame
	void CreateShips()
	{
		pooledShips = new List<GameObject> ();
		for (int i = 0; i < m_shipValues; i++) {
			GameObject obj = (GameObject)Instantiate (objShip);
			obj.SetActive (false);
			obj.transform.SetParent (m_CreateShips.transform);
			pooledShips.Add (obj);
		}
	}
	public GameObject GetPoolBullet()
	{
		
		for (int i = 0; i < pooledObjects.Count; i++) {
			if (!pooledObjects [i].activeInHierarchy) {
				return pooledObjects [i];

			}
		}
	
		return null;
	}
	public GameObject GetPoolShip()
	{
		
		for (int i = 0; i < pooledShips.Count; i++) {
			if (!pooledShips [i].activeInHierarchy) {
				return pooledShips [i];

			}
		}

		return null;
	}
}
