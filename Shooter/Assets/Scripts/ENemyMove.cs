﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENemyMove : MonoBehaviour {
	public Transform shipp;
	public float speed;
	void Start()
	{
		shipp = GameObject.Find ("Ship").transform;
	}
	void Update()
	{
		
	
		transform.position = Vector3.MoveTowards (transform.position, shipp.position, speed * Time.deltaTime);
	}
	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "Player" || col.tag == "Bullet") {
			gameObject.SetActive (false);
		}
	}


}
